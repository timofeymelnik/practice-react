import {combineReducers} from 'redux';
import users from './users';
import map from "./map";

const todoApp = combineReducers({
    users,
    map
});

export default todoApp
