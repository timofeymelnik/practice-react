import React from 'react'
import PropTypes from 'prop-types'

const User = ({name}) => (
    <li>{name}</li>
);

User.propTypes = {
    name: PropTypes.string.isRequired
};

export default User
